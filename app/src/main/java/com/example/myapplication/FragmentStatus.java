package com.example.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

/** tutorial: https://github.com/PhilJay/MPAndroidChart/wiki/Setting-Data **/

public class FragmentStatus extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_status, container,
                false);

        /** create line chart **/
        LineChart lineChart = (LineChart) rootView.findViewById(R.id.status_line_chart);
        List<Entry> line_entries = new ArrayList<>();
        for (int i=0; i<6; i++) {
            // turn your data into Entry objects
            line_entries.add(new Entry(i, 2*i));
        }
        LineDataSet dataSet = new LineDataSet(line_entries, "y = 2 * x");

        LineData lineData = new LineData(dataSet);
        Description desc = new Description();
        desc.setText("Hourly activity");
        desc.setTextSize(14);
        lineChart.setDescription(desc);
        lineChart.setData(lineData);
        lineChart.invalidate(); // refresh

        /** create bar chart **/
        BarChart barChart = (BarChart) rootView.findViewById(R.id.status_bar_chart);
        List<BarEntry> bar_entries = new ArrayList<>();
        bar_entries.add(new BarEntry(0f, 30f));
        bar_entries.add(new BarEntry(1f, 80f));
        bar_entries.add(new BarEntry(2f, 60f));
        bar_entries.add(new BarEntry(3f, 50f));
        // gap of 2f
        bar_entries.add(new BarEntry(5f, 70f));
        bar_entries.add(new BarEntry(6f, 60f));

        BarDataSet set = new BarDataSet(bar_entries, "BarDataSet");

        BarData data = new BarData(set);
        desc = new Description();

        desc.setText("Daily activity");
        desc.setTextSize(14);

        data.setBarWidth(0.9f); // set custom bar width
        barChart.setData(data);
        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.setDescription(desc);
        barChart.invalidate();

        return rootView;
    }

}
