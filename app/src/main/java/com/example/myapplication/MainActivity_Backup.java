package com.example.myapplication;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity_Backup extends AppCompatActivity {

    // page indicators
    public static int MAIN = 0;
    public static int STATUS = 1;
    public static int SETTING = 2;

    private View mMainView;
    private View mStatusView;
    private View mSettingView;
    private int recent_page;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recent_page = MAIN;

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        OnTabSelectListener tbl_select = new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
//                if (tabId == R.id.tab_bottom_main && recent_page != MAIN) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_main);
//                    recent_page = MAIN;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }
//                else if (tabId == R.id.tab_bottom_status && recent_page != STATUS) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_status);
//                    recent_page = STATUS;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }
//                else if (tabId == R.id.tab_bottom_setting && recent_page != SETTING) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_setting);
//                    recent_page = SETTING;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }

            }


        };

        OnTabReselectListener tbl_reselect = new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
//                if (tabId == R.id.tab_bottom_main && recent_page != MAIN) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_main);
//                    recent_page = MAIN;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }
//                else if (tabId == R.id.tab_bottom_status && recent_page != STATUS) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_status);
//                    recent_page = STATUS;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }
//                else if (tabId == R.id.tab_bottom_setting && recent_page != SETTING) {
//                    // change your content accordingly.
//                    setContentView(R.layout.activity_setting);
//                    recent_page = SETTING;
//                    Log.d("table", "onTabSelected: " + recent_page);
//                }
            }
        };

        bottomBar.setOnTabSelectListener(tbl_select);

        bottomBar.setOnTabReselectListener(tbl_reselect);
    }
}
