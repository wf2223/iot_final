package com.example.myapplication;

import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {

    private NoSwipeableViewPager mViewPager;

    private FragmentMain page_main;
    private FragmentStatus page_status;
    private FragmentSetting page_setting;

    private TextView print_area;

    /**
     * setting info
     **/
    private String CollarAddress;
    private String ServerAddress;
    private String DogName;
    private String DogBread;
    private boolean DogGender;  // Is male = true, is female = false
    private String DogAge;


    /** The number of pages (wizard steps) to show in this demo. **/
    private static final int NUM_PAGES = 3;

    private static String mFileName = null;

//    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);

        mViewPager = (NoSwipeableViewPager) findViewById(R.id.pager);

        /** set the adapter for ViewPager */
        mViewPager.setAdapter(new SamplePagerAdapter(
                getSupportFragmentManager()));

        Animation animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide);
        mViewPager.setAnimation(animSlide);

        /** set bottom bar listener **/
        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.selectTabAtPosition(0);
        OnTabSelectListener tbl_select = new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_bottom_main) {
                    mViewPager.setCurrentItem(0);
                    Log.d("table", "onTabSelected: main");
                } else if (tabId == R.id.tab_bottom_status) {
                    mViewPager.setCurrentItem(1);
                    Log.d("table", "onTabSelected: status");
                } else if (tabId == R.id.tab_bottom_setting) {
                    mViewPager.setCurrentItem(2);
                    Log.d("table", "onTabSelected: setting");
                }
            }
        };
        bottomBar.setOnTabSelectListener(tbl_select);

        /** page initialization **/
        page_main = new FragmentMain();
        page_status = new FragmentStatus();
        page_setting = new FragmentSetting();

//        print_area = (TextView) findViewById(R.id.print);
    }

    public class SamplePagerAdapter extends FragmentPagerAdapter {
        public SamplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return page_main;
            } else if (position == 1) {
                return page_status;
            } else {
                return page_setting;
            }
        }


        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public void settingAge(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.dog_info_gender_male:
                if (checked)
                    DogGender = true;
                Log.d("Ratio", "Male");
                break;
            case R.id.dog_info_gender_female:
                if (checked)
                    DogGender = false;
                Log.d("ratio", "Female");
                break;
        }
    }

    public void settingCollarAddress(View view) {
        CollarAddress = ((EditText) findViewById(R.id.collar_address)).getText().toString();
        // checkAddressValidation()
        new GetDataFromPost().execute(CollarAddress, "test connect");
        Log.d("add", CollarAddress);
    }

    public void settingServerAddress(View view) {
        ServerAddress = ((EditText) findViewById(R.id.server_address)).getText().toString();
        // checkAddressValidation()
        new GetDataFromPost().execute(ServerAddress, "test connect");
        Log.d("add", ServerAddress);
    }

    public void settingDogInfo(View view) {
        settingCollarAddress(view);
        settingServerAddress(view);

        DogName = ((EditText) findViewById(R.id.dog_info_name)).getText().toString();
        DogBread = ((EditText) findViewById(R.id.dog_info_breed)).getText().toString();
        DogAge = ((EditText) findViewById(R.id.dog_info_age)).getText().toString();

        // checkAddressValidation()
        Log.d("add", DogName);
        Log.d("add", DogBread);
        Log.d("add", DogAge);

//        new GetDataFromPost().execute(CollarAddress, "test connect");
//        new GetDataFromPost().execute(ServerAddress, "test connect");
    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class GetDataFromPost extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... args) {
            // parameters comes from the execute() call:
            // params[0] is the url
            // params[1] is the commend type
            try {
                String return_text = SendPostRequest(args[0], args[1]);
                if (return_text != "") {
                    return return_text;
                } else {
                    return "Nothing received";
                }

            } catch (Exception e) {
                return "Unable to retrieve web page. Url may be invalid: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
//            print_area.setText(result);
            Log.d("CONN",result);
        }
    }

    // send the post request to the target URL
    @Nullable
    private String SendPostRequest(String myurl, String command) throws IOException {

        // Only display the first 500 characters of the retrieved
        // web page content.
        String response = "";
        OutputStream os;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set up connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Initialize the transmission data
            String message=" ";

            switch (command) {
                case "collar_message":
                    // send voice message to the collar
                    message = "";
                    break;
                case "server_setting_info":
                    // link the dog info to sensor data in server
                    break;
                case "server_live_data":
                    // retrieve current health data and emotion
                    break;
                case "server_daily_data":
                    // retrieve data according to hour
                    break;
                case "server_history_data":
                    // retrieve data in certain time span
                    break;
                default:
                    // connection test
                    message = "{'type': 0}";
                    break;
            }


            // transmit the data to server
            os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(message);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            Log.d("D", "" + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line != null) {
                    response += line;
                    line = br.readLine();
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

//    private void onRecord(boolean start) {
//        if (start) {
//            startRecording();
//        } else {
//            stopRecording();
//        }
//    }
//
//    private void startRecording() {
//        mRecorder = new MediaRecorder();
//        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        mRecorder.setOutputFile(mFileName);
//        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//
//        try {
//            mRecorder.prepare();
//        } catch (IOException e) {
//            Log.e("Media", "prepare() failed");
//        }
//
//        mRecorder.start();
//    }
//
//    private void stopRecording() {
//        mRecorder.stop();
//        mRecorder.release();
//        mRecorder = null;
//    }
//
//    class RecordButton extends Button {
//        boolean mStartRecording = true;
//
//        View.OnClickListener clicker = new View.OnClickListener() {
//            public void onClick(View v) {
//                onRecord(mStartRecording);
//                if (mStartRecording) {
//                    setText("Stop recording");
//                } else {
//                    setText("Start recording");
//                }
//                mStartRecording = !mStartRecording;
//            }
//        };
//
//        public RecordButton(Context ctx) {
//            super(ctx);
//            setText("Start recording");
//            setOnClickListener(clicker);
//        }
//    }

}
