package com.example.myapplication;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import static android.view.MotionEvent.*;

public class FragmentMain extends Fragment {
    TransitionDrawable transition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main, container,
                false);

        ImageView img_view = (ImageView) rootView.findViewById(R.id.img_dog);
        transition = (TransitionDrawable) img_view.getBackground();

        img_view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case ACTION_DOWN:
                        transition.startTransition(1000);
                        break;
                    case ACTION_UP:
                        transition.reverseTransition(1000);
                        break;
                }
                return true;
            }
        });
        return rootView;
    }

}