package com.example.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FragmentSetting extends Fragment {

    EditText dog_info_name;
    EditText dog_info_breed;
    EditText dog_info_age;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_setting, container,
                false);
        dog_info_name = ((EditText)rootView.findViewById(R.id.dog_info_name));
        dog_info_breed = ((EditText)rootView.findViewById(R.id.dog_info_breed));
        dog_info_age = ((EditText)rootView.findViewById(R.id.dog_info_age));

        return rootView;
    }

    // need to add function that retrieve the data from activity

}